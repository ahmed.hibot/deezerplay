from multiprocessing.managers import BaseManager
from keras.models import Sequential, Model
from keras.layers import Embedding, Reshape, Activation, Input, Dense
from keras.layers.merge import Dot
from keras.utils import np_utils
from keras.preprocessing.sequence import skipgrams
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import pdist
from sklearn.neighbors import NearestNeighbors
from flask import Flask, request, session, make_response, jsonify
import ast
import json
import numpy as np
import pandas as po
import random


app = Flask(__name__)
app.secret_key = '123'

def predict(seeds,s,X):
    nbrs = NearestNeighbors(n_neighbors=s + 1, algorithm='ball_tree').fit(X)
    distances, indices = nbrs.kneighbors(X[seeds,:])
    return indices

def get_play_tst():
    lines = []
    with open('play_tst.txt', 'r') as play_tst_file:
        lines = [line.rstrip('\n') for line in play_tst_file]
    lines = filter(lambda x: len(x) > 0 and len(x[0]), lines)
    play_tst = [ [int(track) for track in line.split(' ') if track is not ''] for line in lines]
    return play_tst

def get_vectors_tracks():
    lines = []
    with open('vectors_tracks.txt', 'r') as vectors_tracks_file:
        lines = [line.rstrip('\n') for line in vectors_tracks_file]
    lines = filter(lambda x: len(x) > 0 and len(x[0]), lines)
    vectors_tracks = [ [float(track) for track in line.split(' ') if track is not ''] for line in lines]
    return vectors_tracks

def get_tracks_csv(pr):
    manager = BaseManager(address=('127.0.0.1', 5000), authkey=b'abc')
    manager.register('get_trj_meta_json')
    manager.connect()
    return manager.get_trj_meta_json(pr)

def get_track_by_name(name):
    manager = BaseManager(address=('127.0.0.1', 5000), authkey=b'abc')
    manager.register('get_track')
    manager.connect()
    return manager.get_track(name)

@app.route("/playlist", methods=['POST'])
def get_recommended_playlist():
    nb_result = 5
    seeds = request.get_json()
    if seeds is None or seeds['seeds'] is None:
        return json.dumps(dict()), 200
    vector_tracks = np.asarray(get_vectors_tracks())
    pr = predict(seeds['seeds'], nb_result, vector_tracks)
    tracks = get_tracks_csv(pr[0])
    #print(pr)
    print(tracks)
    #for p in pr:
    #    for a in p:
    #        ret.append((trj_meta.to_dict()['title'][a]))
    tracks = tracks.fillna(0)
    tracks = tracks.to_dict()
    print(tracks['artist'])    
    for artist_nb in tracks['artist']:
        print(artist_nb)
        print(ast.literal_eval(tracks['artist'][artist_nb]))
        tracks['artist'][artist_nb] = ast.literal_eval(tracks['artist'][artist_nb])
    # tracks = tracks.artist.radio.astype(int)
    return json.dumps(tracks), 200

    
@app.route("/searchTracks")
def search_tracks():
    name = request.args.get('name')
    tracks = get_track_by_name(name)
    tracks = tracks.fillna(0)
    return json.dumps(tracks.to_dict()), 200

@app.route("/hitat10")
def hitat10():
    vectors_tracks = np.asarray(get_vectors_tracks())
    play_tst = get_play_tst()
    # estimation des performances
    # hit@10
    # nombre de bonne prediction
    goodpred = []
    # nombre de predictions faites
    nbpred   = []
    i = 0
    nb_play_tst = str(len(play_tst))
    # pour chaque playlist
    print("NUMBER OF PLAYLISTS: " + nb_play_tst)
    for p in play_tst:
        # si au moins deux chansons
        if i % 1000 == 0:
            print(str(i) + "/" + nb_play_tst)
        if (len(p)>1):
            i += 1
            # recuperations des seeds 5 premiers morceaux ou moins si la playlist contient moins de 5 morceaux
            seeds  = p[:np.min([5,len(p)-1])]
            # recuperations de la suite de la playlist que nous allons comparer à nos suggestions
            topred = p[np.min([5,len(p)-1]):]
            # construction des suggestions 10 suggestions par morceaux a predire
            prediction = predict(seeds,10*len(topred), vectors_tracks)
            # comptage du nombre de morceaux présent dans nos suggestions
            goodpred.append(len(np.intersect1d(prediction,topred)))
            # stockage du nombre de predictions
            nbpred.append(len(topred))
            
    # proportions de morceux présents dans nos suggestions
    hitat10 = np.sum(goodpred)/np.sum(nbpred)
    return json.dumps({"hitat10": hitat10}), 200

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run(port = 3000)