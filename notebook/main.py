import numpy as np
import pandas as po
import random
from keras.models import Sequential, Model
from keras.layers import Embedding, Reshape, Activation, Input, Dense
from keras.layers.merge import Dot
from keras.utils import np_utils
from keras.preprocessing.sequence import skipgrams
from sklearn.metrics import pairwise_distances
from scipy.spatial.distance import pdist
from sklearn.neighbors import NearestNeighbors
from flask import Flask, request, session, make_response, jsonify
from flask_session import Session
from multiprocessing import Lock
from multiprocessing.managers import BaseManager 
import json


#app.config.from_object(__name__)

# hyper-paramètres de word2vec :
# dimension de l'espace latent
vector_dim = 50
# taille de la fenêtre de voisinage
window_width = 5
# sur-échantillonage des exemples négatifs
neg_sample = 4.
# taille des mini-batch
min_batch_size = 50
# coeff pour la loi de tirage des exemple negatif
samp_coef = -0.1

# chargement des données de playlist
def load_datas_playlist():
    data = np.load("./music_2.npy")
    tracks_csv = po.read_csv("./Tracks.csv")
    return data, tracks_csv
    
# nombre d'occurence de chaque morceaux ?
def track_count_each(Vt, playlist_track, tracks):
    track_counts = dict((tracks[i],0) for i in range(0, Vt))
    for p in playlist_track:
        for a in p:
            track_counts[a]=track_counts[a] + 1
    return track_counts
    
def filter_non_relevant_tracks(track_counts, playlist_track):
    # filtrage des morceaux très peu fréquents
    playlist_track_filter = [list(filter(lambda a : track_counts[a]> 10, playlist)) for playlist in playlist_track]
    track_f = np.unique(np.concatenate(playlist_track_filter))
    Vt = len(track_f)
    return playlist_track_filter, track_f, Vt

def convert_playlist_to_int(Vt, track_f, playlist_track_filter):
    # construction d'un dict id_morceaux id [0,Vt]
    track_dict = dict((track_f[i],i) for i in range(0, Vt))
    # conversion des playlisat en liste d'entier
    corpus_num_track = [[track_dict[track] for track in play ] for play in playlist_track_filter]
    return corpus_num_track, track_dict

def construct_table(track_f, Vt, playlist_track_filter):
    # comptage du nombre d'occurences des morceaux dans les playlist filtrées
    tracks_counts_f = dict((track_f[i],0) for i in range(0, Vt))
    for p in playlist_track_filter:
        for t in p:
            tracks_counts_f[t]=tracks_counts_f[t]+1
    # construction de la table de tirage des morceaux pour les exmeple negatif en utilisant ces fréquences
    spt_tracks=np.array(list(map(lambda a:tracks_counts_f[a],track_f)),np.float)
    sptn_tracks=np.power(spt_tracks,samp_coef)
    sptn_tracks=sptn_tracks/np.sum(sptn_tracks)
    sptn_tracks=np.cumsum(np.sort(sptn_tracks)[::-1])
    return tracks_counts_f, sptn_tracks

def word2vec(Vt):
    # construction du réseau word2vec
    # entrée deux entier (couple de morceaux)
    input_target_t = Input((1,), dtype='int32')
    input_context_t = Input((1,), dtype='int32')

    # définition de l'embeding
    embedding_t_t = Embedding(Vt, vector_dim, input_length=1, name='embedding_t')
    # projection du premier morceau
    target_t = embedding_t_t(input_target_t)
    target_t = Reshape((vector_dim, 1))(target_t)

    # projection du second morceaux
    context_t = embedding_t_t(input_context_t)
    context_t = Reshape((vector_dim, 1))(context_t)

    # calcul de la sortie
    dot_product_t = Dot(axes=0)([target_t, context_t])
    dot_product_t = Reshape((1,))(dot_product_t)
    output_t = Dense(1, activation='sigmoid',name="classif")(dot_product_t)

    # definition du modèle
    SkipGram_t = Model(inputs=[input_target_t, input_context_t], outputs=output_t)
    SkipGram_t.compile(loss='binary_crossentropy', optimizer='adam')
    return SkipGram_t

# définition du générateur de couple de morceaux (y=0 <-> aléatoire, y=1 <-> proche dans une playlist)
def track_ns_generator(corpus_num, nbm, Vt, sptn_tracks):
    while 1:
        Data=[]
        Labels=[]
        for i, doc in enumerate(random.sample(corpus_num,nbm)):
            data, labels = skipgrams(sequence=doc, vocabulary_size=Vt, window_size=window_width, negative_samples=neg_sample,sampling_table=sptn_tracks)
            if (len(data)>0):
                Data.append(np.array(data, dtype=np.int32))
                Labels.append(np.array(labels, dtype=np.int32))
        Data=np.concatenate(Data)
        Labels=np.concatenate(Labels)
        x=[Data[:,0],Data[:,1]]
        y=Labels
        yield (x,y)


def write_datas_in_file(file_name, vector_datas):
    with open(file_name, 'w') as vector_datas_file:
        for playlist in vector_datas:
            for track in playlist:
                vector_datas_file.write(str(track) + " ")
            vector_datas_file.write("\n")

def get_trj_meta_json(track_ids):
    print('getting trj_meta_json: ')
    return main.trj_meta.loc[track_ids, ["title", "preview", "artist"]]

def get_track(track_name):
    return main.trj_meta[main.trj_meta['title'].str.contains(track_name, case=False)]

def main():
    data, tracks_csv = load_datas_playlist()
    print("nombre de musiques: " + str(len(data)))
    # récupération uniquement des identifiant de morceaux // suppression des identifiant d'artiste
    playlist_track = [list(filter(lambda w: w.split("_")[0]==u"track",playlist)) for playlist in data]
    # nombre de morceaux != ?
    tracks = np.unique(np.concatenate(playlist_track))
    Vt = len(tracks)
    track_counts = track_count_each(Vt, playlist_track, tracks)
    playlist_track_filter, track_f, Vt = filter_non_relevant_tracks(track_counts, playlist_track)
    corpus_num_track, track_dict = convert_playlist_to_int(Vt, track_f, playlist_track_filter)
    tracks_counts_f, sptn_tracks = construct_table(track_f, Vt, playlist_track_filter)
    SkipGram_t = word2vec(Vt)
    # ensemble de test et d'apprentissage
    index_tst = np.random.choice(100000,10000)
    index_app  = np.setdiff1d(range(100000),index_tst)
    print("corpus:")    
    print(corpus_num_track[0])
    play_app = [corpus_num_track[i] for i in index_app]
    play_tst = [corpus_num_track[i] for i in index_tst]
    # apprentissage
    nb_epoch = 5
    hist=SkipGram_t.fit_generator(track_ns_generator(play_app,min_batch_size, Vt, sptn_tracks),200, nb_epoch)
    vectors_tracks = SkipGram_t.get_weights()[0]
    write_datas_in_file('play_tst.txt', play_tst)
    write_datas_in_file('vectors_tracks.txt', vectors_tracks)
    jdf = po.DataFrame({"id":track_f,"index":range(Vt)})
    jdf["deezer_id"]=jdf["id"].apply(lambda x: float(x.split("_")[1]))
    main.trj_meta = tracks_csv.merge(jdf, left_on="id",right_on="deezer_id")
    main.trj_meta.set_index("index",inplace=True)
    print(main.trj_meta.head(5))       
    manager = BaseManager(address=('127.0.0.1', 5000), authkey=b'abc')
    manager.register('get_trj_meta_json', get_trj_meta_json)
    manager.register('get_track', get_track)
    server = manager.get_server()
    server.serve_forever()


if __name__ == "__main__":
    main()