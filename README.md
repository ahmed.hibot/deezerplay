# projet word2vec - Deezerplay

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Requirement

* node
* python 3.6

## install project

Run `npm install` to install Angular

Libraries : 
Run `pip install <name>`
* keras
* tensorflow
* matplotlib
* sklearn
* numpy
* pandas


## Run project

Run `npm start`

Run `python3 main.py`

Then, run `python3 api_server.py`

Navigate to `http://localhost:4200/` 

