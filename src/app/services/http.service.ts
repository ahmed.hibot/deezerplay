import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private state  = false;
  private BASE_URL = "http://localhost:8080";
  constructor(private http: HttpClient) { }

  public setState(state: boolean) {
    this.state = state;
  }

  public getState() {
    return this.state;
  }

  public get(url, params) {
    return this.http.get(url, {params: params}).toPromise();
  }

  public post(url, params) {
    return this.http.post(url, params, {headers: {'Content-Type': 'application/json'}}).toPromise();
  }

  public postOptions(url, params, options) {
    return this.http.post(url, params, options).toPromise();    
  }
}
